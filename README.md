﻿# Набор задач на практику №5
## Срок выполнения (07.04-17.04)

* Написать программу, в которой создается структура **BOOK** для хранения информации о названиях книг, 
ФИО авторов и годе издания. Из текстового файла, подготовленного в обычном редакторе, 
считываются данные о книгах в динамический массив структур **BOOK**.

     1. Распечатать данные о книгах на экране
     2. Распечатать данные о самой старой и самой новой книге (по году издания)
     3. Распечатать данные, отсортированные по фамилиям авторов

* Написать программу, для сбора статистических данных о текстовом файле.
 На вход программы подается текстовый файл, а на экран выводятся:

     1. количество символов
     2. количество слов
     3. количество знаков препинания
     4. количество цифр
     5. средняя длина слова
     6. самый популярный символ

Все задачи оформляются в виде отдельных файлов

- task05-1.c
- task05-2.c

Файлы помещаются в каталог с именем студента (латинские буквы!), который, в свою очередь, должен размещаться в каталоге **src** репозитория.