#include <stdio.h>
#include <stdlib.h>
#define N 50

int mas[256];
int main(){
	FILE *fin;
	fin=fopen("input.txt","r");
	char c;
	int sum=0,space=0,znak=0,num=0,word=0,buf=0;
	while((c=getc(fin))!=EOF){
		mas[(int)c]++;
		sum++;
		if (c>='0' && c<='9')
			num++;
		if (c==' '){
			space++;
			word+=buf;
			buf=0;
		}
		else
			buf++;
		if (c=='.' || c==',' || c== ';' || c==':' || c=='?' || c=='!' || c=='"' || c=='\'')
			znak++;
	}
	if (buf>0)
		word+=buf;
	int i,m=0;
	for(i=0;i<255;i++)
		if (mas[i]>m){
			m=mas[i];
			c=i;
		}
	printf("%d %d %d %d %f %c",sum,space+1,znak,num,(double)word/(double)(space+1),c);
	return 0;
}
